<?php

use cursophp7\app\entity\ImageGallery;
use cursophp7\app\exception\AppException;
use cursophp7\app\exception\FileException;
use cursophp7\app\exception\QueryException;
use cursophp7\app\exception\ValidationException;
use cursophp7\app\repository\ImagenGaleriaRepository;
use cursophp7\app\utils\File;
use cursophp7\core\App;




try{

        $descripcion=trim(htmlspecialchars($_POST['descripcion']));
        $categoria=trim(htmlspecialchars($_POST['categoria']));

        if(empty($categoria)){
            throw new ValidationException('No se ha recibido la categoría');
        }
        $tiposAceptados=['image/jpeg','image/png','image/gif'];
        $imagen=new File('imagen',$tiposAceptados);

        $imagen->saveUploadFile(ImageGallery::RUTA_IMAGENES_GALLERY);
        $imagen->copyFile(ImageGallery::RUTA_IMAGENES_GALLERY,ImageGallery::RUTA_IMAGENES_PORTFOLIO);

        $imagenGaleria = new ImageGallery($imagen->getFileName(),$descripcion,$categoria);
        $imgRepository=App::getRepository(ImagenGaleriaRepository::class);
        $imgRepository->guarda($imagenGaleria);

        $message="Se ha guardado una nueva imagen" . $imagenGaleria->getNombre();
        App::get('logger')->add($message);

}
catch(FileException $fileException){

    die($fileException->getMessage());

}
catch(QueryException $queryException){
    die($queryException->getMessage());

}
catch(ValidationException $validationException){
    die($validationException->getMessage());

}
catch(AppException $appException){
    die($appException->getMessage());
}
App::get('router')->redirect('imagenes-galeria');





?>