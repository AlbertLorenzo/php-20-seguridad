<?php
/**
 * Created by PhpStorm.
 * User: daw2
 * Date: 7/12/18
 * Time: 17:15
 */

namespace cursophp7\core;


use cursophp7\app\exception\AppException;

class Response
{
    /**
     * @param $name
     * @param string $layout
     * @param array $data
     * @throws AppException
     */
    public static function renderView($name,$layout='layout',array $data=[])
    {
        extract($data);

        $app['user']=App::get('appUser');

        ob_start();
        require __DIR__."/../app/views/$name.view.php";

        $mainContent=ob_get_clean();
        require __DIR__."/../app/views/$layout.view.php";
    }

}